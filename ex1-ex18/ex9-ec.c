#include <stdio.h>

int main(int argc, char *argv[])
{
    int numbers[4] = {0};
    char name[4] = {'a',};

    // first, print them out raw
    printf("numbers: %d %d %d %d\n",
            numbers[0], numbers[1],
            numbers[2], numbers[3]);

    printf("name each: %c %c %c %c\n",
            name[0], name[1],
            name[2], name[3]);
    
    printf("name: %s\n", name);

    // setup the numbers
    numbers[0] = 1;
    numbers[1] = 2;
    numbers[2] = 3;
    numbers[3] = 4;

    // setup the name
    name[0] = 'Z';
    name[1] = 'e';
    name[2] = 'd';
    name[3] = '\0';

    // then print them out initialized
    printf("numbers: %d %d %d %d\n",
            numbers[0], numbers[1],
            numbers[2], numbers[3]);

    printf("name each: %c %c %c %c\n",
            name[0], name[1],
            name[2], name[3]);

    // print the name like a string
    printf("name: %s\n", name);

    // another way to use name
    char *another = "Zed";

    printf("another: %s\n", another);

    printf("another each: %c %c %c %c\n",
            another[0], another[1],
            another[2], another[3]);

    // print name as pointer style
    char *name_ptr = name;

    printf("name_ptr: %s\n", name_ptr);

    printf("name_ptr each: %c %c %c %c\n",
            name_ptr[0], name_ptr[1],
            name_ptr[2], name_ptr[3]);

    numbers[0] = 'Z';
    numbers[1] = 'e';
    numbers[2] = 'd';
    numbers[3] = '\0';

    // print characters as assigned into numbers
    printf("chars as assigned to numbers: %d %d %d %d\n",
            numbers[0], numbers[1],
            numbers[2], numbers[3]);

    name[0] = 1;
    name[1] = 2;
    name[2] = 3;
    name[3] = 4;

    // print numbers as assigned into name
    printf("numbers as assigned to name: %c %c %c %c\n",
            name[0], name[1],
            name[2], name[3]);

    // treat a 4-char array as an integer
    int shift_mult = 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2;
    int shift_mult_2 = shift_mult * shift_mult;
    int shift_mult_3 = shift_mult_2 * shift_mult;
    int asInt = 'N' * shift_mult_3 + 'i' * shift_mult_2 +
        'b' * shift_mult + 's';
    char fromInt[4];
    fromInt[3] = asInt % shift_mult;
    asInt = asInt / shift_mult;
    fromInt[2] = asInt % shift_mult;
    asInt = asInt / shift_mult;
    fromInt[1] = asInt % shift_mult;
    asInt = asInt / shift_mult;
    fromInt[0] = asInt;
    printf("decomposed int rep of 4-char array: %c %c %c %c\n",
            fromInt[0], fromInt[1],
            fromInt[2], fromInt[3]);

    return 0;
}
