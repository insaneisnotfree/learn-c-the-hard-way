#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

struct Address {
    int id;
    int set;
    char *name;
    char *email;
};

struct Database {
    int max_data;
    int max_rows;
    struct Address **rows;
};

struct Connection {
    FILE *file;
    struct Database *db;
};

// Forward declaration to allow use in die()
void Database_close(struct Connection *conn);

void die(const char *message, struct Connection *conn)
{
    if (errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    if (conn) {
        Database_close(conn);
    }

    exit(1);
}

void Address_print(struct Address *addr)
{
    printf("%d %s %s\n",
           addr->id, addr->name, addr->email);
}

struct Address *Address_load(struct Connection *conn, int max_data) 
{
    struct Address *addr = malloc(sizeof(struct Address));

    int rc = fread(&addr->id, sizeof(int), 1, conn->file);
    if (rc != 1) { die("Error reading address id", conn); }

    rc = fread(&addr->set, sizeof(int), 1, conn->file);
    if (rc != 1) { die("Error reading set flag", conn); }

    char *name_ptr = malloc(sizeof(char) * max_data);
    rc = fread(name_ptr, sizeof(char) * max_data, 1, conn->file);
    name_ptr[max_data - 1] = '\0';
    addr->name = name_ptr;

    char *email_ptr = malloc(sizeof(char) * max_data);
    rc = fread(email_ptr, sizeof(char) * max_data, 1, conn->file);
    email_ptr[max_data - 1] = '\0';
    addr->email = email_ptr;

    return addr;
} 

void Address_close(struct Address *addr)
{
    if (addr->name) {
        free(addr->name);
    }

    if (addr->email) {
        free(addr->email);
    }

    free(addr);
} 

void Database_load(struct Connection *conn)
{
    // Read the max_data and max_rows from the file
    int max[2];
    int rc = fread(max, sizeof(int), 2, conn->file);
    if (rc != 2) {
        die("Failed to read maxes from file.", conn);
    }

    printf("Database_load got max_data=%d, max_rows=%d\n", max[0], max[1]);
    
    // Read addresses from file using file size
    int max_rows = max[1];
    struct Address **rows = malloc(sizeof(struct Address *) * max_rows);
    int i;
    for (i = 0; i < max_rows; ++i) {
        rows[i] = Address_load(conn, max[0]);
    }

    conn->db->max_data = max[0];
    conn->db->max_rows = max_rows;
    conn->db->rows = rows;
}

struct Connection *Database_open(const char *filename, char mode)
{
    printf("Attempting to allocate connection\n");
    struct Connection *conn = malloc(sizeof(struct Connection));
    if (!conn) die("Memory error", conn);

    conn->db = malloc(sizeof(struct Database));
    if (!conn->db) die("Memory error", conn);

    if (mode == 'c') {
        conn->file = fopen(filename, "w");
    } else {
        conn->file = fopen(filename, "r+");

        if (conn->file) {
            Database_load(conn);
        }
    }

    if (!conn->file) die("Failed to open the file", conn);

    return conn;
}

void Database_close(struct Connection *conn)
{
    if (conn) {
        // Close the file stream
        if (conn->file) fclose(conn->file);
         
        // Free heap memory for each row in db
        int i;
        for (i = 0; i < conn->db->max_rows; ++i) {
            if (conn->db->rows[i]) {
                Address_close(conn->db->rows[i]);
            }    
        }

        if (conn->db->rows) free(conn->db->rows);
        if (conn->db) free(conn->db);
        free(conn);
    }
}

void Database_write_row(struct Connection *conn, struct Address *row)
{
    // Write id and set to file
    int *id_ptr = &row->id;
    int rc = fwrite(id_ptr, sizeof(int), 1, conn->file);
    if (rc != 1) die ("Failed to write id for row", conn);
    rc = fwrite(&row->set, sizeof(int), 1, conn->file);
    if (rc != 1) die ("Failed to write set flag for row", conn);
    
    // Write name and email to file
    int max_data = conn->db->max_data;
    rc = fwrite(row->name, sizeof(char) * max_data, 1, conn->file);
    if (rc != 1) die ("Failed to write name for row", conn);
    rc = fwrite(row->email, sizeof(char) * max_data, 1, conn->file);
    if (rc != 1) die ("Failed to write name for email", conn);
}

void Database_write(struct Connection *conn)
{
    rewind(conn->file);

    // Write the max_data and max_rows to file first
    int *max_data_ptr = &conn->db->max_data;
    printf("max_data_ptr val=%d\n", *max_data_ptr);
    int rc = fwrite(max_data_ptr, sizeof(int), 1, conn->file);
    if (rc != 1) die("Failed to write max_data.", conn);
    rc = fwrite(&conn->db->max_rows, sizeof(int), 1, conn->file);
    if (rc != 1) die("Failed to write max_rows.", conn);

    printf("Successfully wrote max_data=%d and max_rows=%d to file\n", 
            conn->db->max_data, conn->db->max_rows);

    // Write each individual row to db
    int i = 0;
    for (i = 0; i < conn->db->max_rows; ++i) {
        struct Address *row = conn->db->rows[i];
        printf("Writing row with id=%d, set=%d\n", row->id, row->set);
        Database_write_row(conn, row);                    
    }

    rc = fflush(conn->file);
    if (rc == -1) die("Cannot flush database.", conn);
}

char *empty_string(int max_data) 
{
    char *empty_ptr = malloc(sizeof(char) * max_data);
    int i = 0;
    for (i = 0; i < max_data - 1; ++i) {
        empty_ptr[i] = '0';
    }
    empty_ptr[max_data - 1] = '\0';
    return empty_ptr;
}

void Database_create(struct Connection *conn, int max_data, int max_rows)
{
    conn->db->max_data = max_data;
    conn->db->max_rows = max_rows;

    struct Address **rows = malloc(sizeof(struct Address *) * max_rows);
    conn->db->rows = rows;

    int i = 0;

    for (i = 0; i < conn->db->max_rows; ++i) {
        // make a prototype to initialize it
        char *name_ptr = empty_string(max_data);
        char *email_ptr = empty_string(max_data);
        struct Address *addr = malloc(sizeof(struct Address));
        addr->id = i;
        addr->set = 0;
        addr->name = name_ptr;
        addr->email = email_ptr;
        // then just assign it
        conn->db->rows[i] = addr;
    }
}

void Database_set(struct Connection *conn, int id, const char *name, const char *email)
{
    int max_data = conn->db->max_data;

    struct Address *addr = conn->db->rows[id];
    if (addr->set) die("Already set, delete it first", conn);

    addr->set = 1;
    // WARNING: bug, read the "How To Break It" and fix this
    char *res = strncpy(addr->name, name, max_data);
    // demonstrate the strncpy bug
    if (!res) die("Name copy failed", conn);

    res = strncpy(addr->email, email, max_data);
    if (!res) die("Email copy failed", conn);
}

void Database_get(struct Connection *conn, int id)
{
    struct Address *addr = conn->db->rows[id];

    if (addr->set) {
        Address_print(addr);
    } else {
        die("ID is not set", conn);
    }
}

void Database_delete(struct Connection *conn, int id)
{
    char *name_ptr = empty_string(conn->db->max_data);
    char *email_ptr = empty_string(conn->db->max_data);
    struct Address *addr = malloc(sizeof(struct Address));
    addr->id = id;
    addr->set = 0;
    addr->name = name_ptr;
    addr->email = email_ptr;

    Address_close(conn->db->rows[id]);

    conn->db->rows[id] = addr;
}

void Database_list(struct Connection *conn)
{
    int i = 0;
    struct Database *db = conn->db;

    for (i = 0; i < conn->db->max_rows; ++i) {
        struct Address *cur = db->rows[i];

        if (cur->set) {
            Address_print(cur);
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3) die("USAGE: ex17 <dbfile> <action> [action params]", NULL);

    char *filename = argv[1];
    char action = argv[2][0];
    printf("Had filename=%s, action=%c\n", filename, action);

    struct Connection *conn = Database_open(filename, action);
    int id = 0;

    if (argc > 3) id = atoi(argv[3]);

    printf("id=%d\n", id);
    if (action != 'c') {
        printf("max_rows=%d, max_data=%d\n", conn->db->max_rows, conn->db->max_data);    
    }

    switch (action) {
        case 'c':
            if (argc != 5) die ("Need max_data, max_rows to create db", conn);

            Database_create(conn, atoi(argv[3]), atoi(argv[4]));
            printf("Successfully created db\n");
            Database_write(conn);
            break;

        case 'g':
            if (argc != 4) die("Need an id to get", conn);

            if (id >= conn->db->max_rows) die("There's not that many records.", conn);

            Database_get(conn, id);
            break;

        case 's':
            if (argc != 6) die("Need id, name, email to set", conn);

            if (id >= conn->db->max_rows) die("There's not that many records.", conn);

            Database_set(conn, id, argv[4], argv[5]);
            Database_write(conn);
            break;

        case 'd':
            if (argc != 4) die("Need id to delete", conn);

            if (id >= conn->db->max_rows) die("There's not that many records.", conn);

            Database_delete(conn, id);
            Database_write(conn);
            break;

        case 'l':
            Database_list(conn);
            break;
        
        default:
            die("Invalid action, only: c=create, g=get, s=set, d=del, l=list", conn);
    }

    Database_close(conn);

    return 0;
} 
