#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/** Our old friend die from ex17. */
void die (const char *message)
{
    if (errno) {
        perror(message);
    } else {
        printf("ERROR: %s\n", message);
    }

    exit(1);
}

// a typedef creates a fake type, in this
// case for a function pointer
typedef int (*compare_cb)(int a, int b);

// a typedef for sort function pointers
typedef int *(*sort_algo)(int *numbers, int count, compare_cb cmp);

/**
 * A classic bubble sort function that uses the
 * compare_cb to do the sorting.
 */
int *bubble_sort(int *numbers, int count, compare_cb cmp)
{
    int temp = 0;
    int i = 0;
    int j = 0;
    int *target = malloc(count * sizeof(int));

    if (!target) die("Memory error.");

    memcpy(target, numbers, count * sizeof(int));

    for (i = 0; i < count; ++i) {
        for (j = 0; j < count - 1; ++j) {
            if (cmp(target[j], target[j+1]) > 0) {
                temp = target[j+1];
                target[j+1] = target[j];
                target[j] = temp;
            }
        }
    }

    return target;
}

int *quicksort(int *target, int count, compare_cb cmp)
{
    if (count <= 1) {
        int *same = malloc(count * sizeof(int));
        if (count == 1) {
            same[0] = target[0];
        }
        return same;
    }

    // Pick the middle element of the array as the pivot
    int pivot_idx = count / 2;
    int pivot_val = target[pivot_idx];
    
    // Assign elements to go in the less and greater lists
    int *less = malloc(count * sizeof(int));
    int less_count = 0;
    int *greater = malloc(count * sizeof(int));
    int greater_count = 0;
    int i = 0;
    for (i = 0; i < count; ++i) {
        if (i == pivot_idx) {
            continue;
        }

        if (cmp(target[i], pivot_val) <= 0) {
            less[less_count++] = target[i];
        } else {
            greater[greater_count++] = target[i];
        }
    }

    int *sorted_less = quicksort(less, less_count, cmp);
    int *sorted_greater = quicksort(greater, greater_count, cmp);

    free(less);
    free(greater);

    int *sorted = malloc(count * sizeof(int));
    int sorted_idx = 0;
    for (i = 0; i < less_count; ++i) {
        sorted[sorted_idx++] = sorted_less[i];
    }
    sorted[sorted_idx++] = pivot_val;
    for (i = 0; i < greater_count; ++i) {
        sorted[sorted_idx++] = sorted_greater[i];
    }

    free(sorted_less);
    free(sorted_greater);   

    // malloc'd pointer must be freed by calling function
    return sorted;
}

int sorted_order(int a, int b)
{
    return a - b;
}

int reverse_order(int a, int b)
{
    return b - a;
}

int strange_order(int a, int b)
{
    if (a == 0 || b == 0) {
        return 0;
    } else {
        return a % b;
    }
}

/*
int bad_compare(int a, int b, int c)
{
    return a + b + c;
}
*/

/**
 * Used to test that we are sorting things correctly
 * by doing the sort and printing it out.
 */
void test_sorting(int *numbers, int count, sort_algo sort, compare_cb cmp)
{
    int i = 0;
    int *sorted = sort(numbers, count, cmp);

    if (!sorted) die("Failed to sort as requested.");

    for (i = 0; i < count; ++i) {
        printf("%d ", sorted[i]);
    }
    printf("\n");

    free(sorted);
}

int main(int argc, char *argv[])
{
    if (argc < 2) die("USAGE: ex18 4 3 1 5 6");

    int count = argc - 1;
    int i = 0;
    char **inputs = argv + 1;

    int *numbers = malloc(count * sizeof(int));
    if (!numbers) die("Memory error");

    for (i = 0; i < count; ++i) {
        numbers[i] = atoi(inputs[i]);
    }

    test_sorting(numbers, count, bubble_sort, sorted_order); 
    test_sorting(numbers, count, bubble_sort, reverse_order);
    test_sorting(numbers, count, bubble_sort, strange_order);
    // test_sorting(numbers, count, bad_compare);

    test_sorting(numbers, count, quicksort, sorted_order);
    test_sorting(numbers, count, quicksort, reverse_order);
    test_sorting(numbers, count, quicksort, strange_order);

    free(numbers);

    return 0;
}
