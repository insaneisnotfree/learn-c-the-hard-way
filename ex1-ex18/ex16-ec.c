#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

// Defines a struct named Person with various kinds of member fields/data
struct Person {
    // Defines name field as a pointer to array of characters
    char *name;
    // Lines below define age/height/weight fields on Person as int types
    int age;
    int height;
    int weight;
};

// Creates new Person struct on stack and assigns its fields passed-in values
struct Person Person_create(char *name, int age, int height, int weight)
{
    // Creates a Person struct "who" on stack
    struct Person who;

    // Initializes all the fields on "who" (name should point to string on stack) 
    who.name = name;
    who.age = age;
    who.height = height;
    who.weight = weight;

    // Returns the pointer to the newly-initialized Person struct
    return who;
}

// Prints out the details of a Person struct with some tab formatting
void Person_print(struct Person who)
{
    printf("Name: %s\n", who.name);
    printf("\tAge: %d\n", who.age);
    printf("\tHeight: %d\n", who.height);
    printf("\tWeight: %d\n", who.weight);
}

int main(int argc, char *argv[])
{
    // make two people structs
    struct Person joe = Person_create(
            "Joe Alex", 32, 64, 140);

    struct Person frank = Person_create(
            "Frank Blank", 20, 72, 180);

    // print them out and where they are in memory
    printf("Joe is at memory location %p:\n", &joe);
    Person_print(joe);

    printf("Frank is at memory location %p:\n", &frank);
    Person_print(frank);

    // make everyone age 20 years and print them again
    joe.age += 20;
    joe.height -= 2;
    joe.weight += 40;
    Person_print(joe);

    frank.age += 20;
    frank.weight += 20;
    Person_print(frank);

    return 0;
}
