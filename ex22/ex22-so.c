int infinite_recurse(int i)
{
    return infinite_recurse(++i);
}

int main()
{
    infinite_recurse(0);
}
