#include <stddef.h>

#ifndef _abstr_h
#define _abstr_h

#define MAX_CSTR_LEN 500
#define ABSTR_ERR -1
#define ABSTR_OK 0

struct abstr {
    char *content;
    int len;
};

typedef struct abstr abstr;

abstr abfromcstr(const char *cstr);
int abinstr(const abstr *src, const int start, const abstr *to_find);
abstr empty_abstr(const int buffer_size);
void abstr_free(abstr * const to_free);
int abstr_realloc(abstr * const to_realloc, const size_t new_size);
void abtrimws(abstr * const to_trim);

/// Prototype for a getc() function
typedef int (*abNgetc) (void *parm);

abstr abgets(abNgetc getc_ptr, void *parm, char terminator);
int abgetsa(abstr *str, abNgetc getc_ptr, void *parm, char terminator);

#endif
