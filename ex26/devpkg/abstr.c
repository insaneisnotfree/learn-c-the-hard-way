#include <stdlib.h>
#include "dbg.h"
#include "abstr.h"

abstr abfromcstr(const char *cstr)
{
    int len = 0;
    for (int i = 0; i < MAX_CSTR_LEN; ++i) {
        if (cstr[i] == '\0') {
            len = i+1;
            break;
        }
    }

    abstr data = { .content = NULL, .len = len };

    check(len != 0, "cstr was too long to convert!");

    data.content = malloc(sizeof(char) * len);
    memcpy(data.content, cstr, len * sizeof(char));
    check(data.content != NULL, "Data content was null after memcpy");
    return data;

    error:
        if (data.content != NULL) { free(data.content); }
        data.len = 0;
        return data;
}

int abinstr(const abstr *src, const int start, const abstr *to_find)
{
    if (to_find->len <= 0 || to_find->len > src->len) { return ABSTR_ERR; }

    int find_idx = 0;
    for (int i = 0; i < src->len; ++i) {
        if (src->content[i] == to_find->content[find_idx]) {
            if (find_idx == to_find->len - 1) { // Matched last non-NULL char on to_find
                return i - find_idx;
            } else { // Matched char *not* at last index of to_find
                ++find_idx;
            }
        } else { // Found a non-matched char in to_find, reset find_idx
            find_idx = 0;
        }
    }

    return ABSTR_ERR; // No match found
}

abstr empty_abstr(const int buffer_size)
{
    abstr empty = { .content = malloc(sizeof(char) * buffer_size), .len = -1 };
    return empty;
}

void abstr_free(abstr * const to_free)
{
    if (to_free->content != NULL) { free(to_free->content); }
    to_free->len = 0;
}

int abstr_realloc(abstr * const to_realloc, const size_t new_size)
{
    if (new_size < to_realloc->len) { return -1; }

    to_realloc->content = realloc(to_realloc->content, new_size);
    if (to_realloc->content != NULL) {
        return ABSTR_OK;
    } else {
        return ABSTR_ERR;
    }
}

void abtrimws(abstr * const to_trim)
{
    // Find lowest index with all whitespace chars before '\0'
    int ws_idx = -1;
    for (int i = 0; i < to_trim->len; ++i) {
        const char c = to_trim->content[i];
        if (c == '\0') {
            break;
        } else if (c == ' ' || c == '\t') {
            ws_idx = ws_idx == -1 ? i : ws_idx;
        } else {
            ws_idx = -1;
        }
    }

    if (ws_idx != -1) {
        to_trim->content[ws_idx] = '\0';
    }
}

abstr abgets(abNgetc getc_ptr, void *parm, char terminator)
{
    int ret;
    abstr buff = { .content = NULL, .len = 0};

    if (getc_ptr == NULL) { return buff; }
    buff = abfromcstr("");
    if (buff.content == NULL) { return buff; }

    ret = abgetsa(&buff, getc_ptr, parm, terminator);
    if (ret < 0 || buff.len <= 0) {
        abstr_free(&buff);
        buff.content = NULL;
        buff.len = 0;
    }
    return buff;
}

int abgetsa(abstr *str, abNgetc getc_ptr, void *parm, char terminator)
{
    int c;
    int mlen = str->len, slen = 0;
    for (int i = 0; i < str->len; ++i) {
        if (str->content[i] == '\0') {
            break;
        } else {
            ++slen;
        }
    }

    if (str == NULL || str->len <= 0 || getc_ptr == NULL) {
        return ABSTR_ERR;
    }

    while ((c = getc_ptr(parm)) >= 0) {
        if (slen > mlen - 2) {
            str->len = slen + 2;
            if (abstr_realloc(str, str->len) != ABSTR_OK) { return ABSTR_ERR; }
            mlen = str->len;
        }
        str->content[slen] = (unsigned char) c;
        ++slen;
        if (c == terminator) { break; }
    }

    str->content[slen++] = (unsigned char) '\0';
    str->len = slen;

    return slen == 0 && c < 0;
}
