#include <unistd.h>
#include <linux/limits.h>
#include <apr_errno.h>
#include <apr_file_io.h>

#include "db.h"
#include "abstr.h"
#include "dbg.h"

static FILE *DB_open(const char *path, const char *mode)
{
    return fopen(path, mode);
}


static void DB_close(FILE *db)
{
    fclose(db);
}

static abstr DB_load(size_t size_guess)
{
    FILE *db = NULL;
    abstr data = empty_abstr(sizeof(char) * size_guess);

    db = DB_open(DB_FILE, "r");
    check(db, "Failed to open database: %s", DB_FILE);

    size_t chars_read = 0;
    for (int i = 1; i <= MAX_LOADS; ++i) {
        const size_t nmemb = size_guess * i;
        chars_read = fread(data.content, sizeof(char), nmemb, db);
        if (chars_read <= nmemb) { // We successfully read whole file
            data.len = chars_read;
            break;
        } else { // Resize the *char in abstr to larger buffer and read again
            int rc = abstr_realloc(&data, (i + 1) * size_guess);
            check(rc != -1, "Failed to expand data buffer to read file.");
        }
    }

    check(data.len >= 0, "Failed to read from db file: %s", DB_FILE);
    check(data.content != NULL, "After non-zero read data was NULL");

    DB_close(db);
    return data;

    error:
        if (db) { DB_close(db); }
        if (data.content != NULL) { abstr_free(&data); }
        data.len = -1;
        return data;
}

int DB_update(const char *url)
{
    if (DB_find(url)) {
        log_info("Already recorded as installed: %s", url);
    }

    FILE *db = DB_open(DB_FILE, "a+");
    check(db, "Failed to open DB file: %s", DB_FILE);

    abstr line = abfromcstr(url);
    check(line.len != -1, "Failed to convert cstr %s", url);
    int rc = fwrite(line.content, line.len, 1, db);
    check(rc == 1, "Failed to append to the db.");

    return 0;

    error:
        if (db) DB_close(db);
        return -1;
}

int DB_find(const char *url)
{
    abstr line = abfromcstr(url);
    check(line.len != -1, "Failed to convert cstr");
    int res = -1;

    abstr data = DB_load(DB_SIZE_GUESS);
    check((data.len > 0 && data.content != NULL), "Failed to load: %s", DB_FILE);

    if (abinstr(&data, 0, &line) == ABSTR_ERR) {
        res = 0;
    } else {
        res = 1;
    }

error: // fallthrough
    abstr_free(&data);
    abstr_free(&line);

    return res;
}

int DB_init()
{
    apr_pool_t *p = NULL;
    apr_pool_initialize();
    apr_pool_create(&p, NULL);

    if (access(DB_DIR, W_OK | X_OK) == -1) {
        apr_status_t rc = apr_dir_make_recursive(DB_DIR,
                APR_UREAD | APR_UWRITE | APR_UEXECUTE |
                APR_GREAD | APR_GWRITE | APR_GEXECUTE, p);
        check(rc == APR_SUCCESS, "Failed to make database dir: %s", DB_DIR);
    }

    if (access(DB_FILE, W_OK) == -1) {
        FILE *db = DB_open(DB_FILE, "w");
        check(db, "Cannot open database: %s", DB_FILE);
        DB_close(db);
    }

    apr_pool_destroy(p);
    return 0;

error:
    apr_pool_destroy(p);
    return -1;
}

int DB_list()
{
    abstr data = DB_load(DB_SIZE_GUESS);
    check(data.len >= 0, "Failed to read load: %s", DB_FILE);

    printf("Trying to read abstr with len=%d\n", data.len);
    printf("%s", data.content);
    if (data.content != NULL) { abstr_free(&data); }
    return 0;

error:
    return -1;
}
