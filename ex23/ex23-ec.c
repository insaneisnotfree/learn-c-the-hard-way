#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include "../dbg.h"

#ifndef DUFFS
#define DUFFS
#define CP() *to++ = *from++;
#define SEVEN_DUFFS(F) case F-0:CP() case F-1:CP() case F-2:CP() case F-3:CP() case F-4:CP() case F-5:CP() case F-6:CP()
#endif

#define COPY_LENGTH 400000000

int normal_copy(char *from, char *to, int count)
{
    int i = 0;

    for (i = 0; i < count; i++) {
        to[i] = from[i];
    }

    return i;
}

int mem_cpy(char *from, char *to, int count)
{
    memcpy(to, from, count);
    return count;
}

int duffs_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0: do { *to++ = *from++;
                        case 7: *to++ = *from++;
                        case 6: *to++ = *from++;
                        case 5: *to++ = *from++;
                        case 4: *to++ = *from++;
                        case 3: *to++ = *from++;
                        case 2: *to++ = *from++;
                        case 1: *to++ = *from++;
                    } while (--n > 0);
        }
    }

    return count;
}

int zeds_device(char *from, char *to, int count)
{
    {
        int n = (count + 7) / 8;

        switch(count % 8) {
            case 0:
            again: *to++ = *from++;

            case 7: *to++ = *from++;
            case 6: *to++ = *from++;
            case 5: *to++ = *from++;
            case 4: *to++ = *from++;
            case 3: *to++ = *from++;
            case 2: *to++ = *from++;
            case 1: *to++ = *from++;
                    if (--n > 0) goto again;
        }
    }

    return count;
}

int valid_copy(char * data, int count, char expects)
{
    int i = 0;
    for (i = 0; i < count; i++) {
        if (data[i] != expects) {
            log_err("[%d] %c != %c", i, data[i], expects);
            return 0;
        }
    }

    return 1;
}

// Create a 32-case Zed's device using macro
int macro_copy(char *from, char *to, int count)
{
    int n = (count + 31) / 32;

    switch(count % 32) {
        case 0:
        again: CP()

        SEVEN_DUFFS(31);
        case 24: CP()
        SEVEN_DUFFS(23);
        case 16: CP()
        SEVEN_DUFFS(15);
        case 8: CP()
        SEVEN_DUFFS(7);

        if (--n > 0) goto again;
    }

    return count;
}

int main(int argc, char *argv[])
{
    char *from = malloc(sizeof(char) * COPY_LENGTH);
    char *to = malloc(sizeof(char) * COPY_LENGTH);
    int rc = 0;

    // setup the from to have some stuff
    memset(from, 'x', COPY_LENGTH);
    // set it to a failure mode
    memset(to, 'y', COPY_LENGTH);
    check(valid_copy(to, COPY_LENGTH, 'y'), "Not initialized right.");

    // setup the time-tracking variables
    clock_t start, end;

    // duffs version
    start = clock();
    rc = duffs_device(from, to, COPY_LENGTH);
    end = clock();
    printf("duffs_copy:\n");
    double cpu_time_used = ((double) (end - start));
    printf("cpu_time_used %f\n\n", cpu_time_used);
    check(rc == COPY_LENGTH, "Duff's device failed: %d", rc);
    check(valid_copy(to, COPY_LENGTH, 'x'), "Duff's device failed copy.");

    // reset
    memset(to, 'y', COPY_LENGTH);

    // my version
    start = clock();
    rc = zeds_device(from, to, COPY_LENGTH);
    end = clock();
    printf("zeds_copy:\n");
    cpu_time_used = ((double) (end - start));
    printf("cpu_time_used %f\n\n", cpu_time_used);
    check(rc == COPY_LENGTH, "Zed's device failed: %d", rc);
    check(valid_copy(to, COPY_LENGTH, 'x'), "Zed's device failed copy.");

    // reset
    memset(to, 'y', COPY_LENGTH);

    // EC - macro version
    start = clock();
    rc = macro_copy(from, to, COPY_LENGTH);
    end = clock();
    printf("macro_copy:\n");
    cpu_time_used = ((double) (end - start));
    printf("cpu_time_used %f\n\n", cpu_time_used);
    check(rc == COPY_LENGTH, "macro_copy failed: %d", rc);
    check(valid_copy(to, COPY_LENGTH, 'x'), "macro_copy failed copy.");

    // reset
    memset(to, 'y', COPY_LENGTH);

    // use normal copy
    start = clock();
    rc = normal_copy(from, to, COPY_LENGTH);
    end = clock();
    printf("normal_copy:\n");
    cpu_time_used = ((double) (end - start));
    printf("cpu_time_used %f\n\n", cpu_time_used);
    check(rc == COPY_LENGTH, "Normal copy failed: %d", rc);
    check(valid_copy(to, COPY_LENGTH, 'x'), "Normal copy failed.");

    // reset
    memset(to, 'y', COPY_LENGTH);

    // use memcpy
    start = clock();
    rc = mem_cpy(from, to, COPY_LENGTH);
    end = clock();
    printf("mem_cpy:\n");
    cpu_time_used = ((double) (end - start));
    printf("cpu_time_used %f\n\n", cpu_time_used);
    check(rc == COPY_LENGTH, "Mem copy failed: %d", rc);
    check(valid_copy(to, COPY_LENGTH, 'x'), "Mem copy failed.");

    return 0;
error:
    return 1;
}
