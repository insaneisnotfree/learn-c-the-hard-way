/** WARNING: This code is fresh and potentially isn't correct yet. */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include "dbg.h"

#define MAX_DATA 100

int read_string(char **out_string, int max_buffer)
{
    *out_string = calloc(1, max_buffer + 1);
    check_mem(*out_string);

    char *result = fgets(*out_string, max_buffer, stdin);
    check(result != NULL, "Input error.");

    /*
    int i = 0;
    for (i = 0; i < max_buffer; i++) {
        char input_char = fgetc(stdin);
        if (input_char == '\n') {
            // BUG: forgets null terminator
            break;
        } else {
            out_string[0][i] = input_char;
        }
    }
    */

    return 0;

    error:
    if (*out_string) free(*out_string);
    *out_string = NULL;
    return -1;
}

int read_int(int *out_int)
{
    char *input = NULL;
    int rc = read_string(&input, MAX_DATA);
    check(rc == 0, "Failed to read number.");

    *out_int = atoi(input);

    free(input);
    return 0;

    error:
    if (input) free(input);
    return -1;
}

int read_scan(const char *fmt, ...)
{
    int i = 0;
    int rc = 0;
    int *out_int = NULL;
    char *out_char = NULL;
    char **out_string = NULL;
    int max_buffer = 0;

    va_list argp;
    va_start(argp, fmt);

    for (i = 0; fmt[i] != '\0'; i++) {
        if (fmt[i] == '%') {
            i++;
            switch(fmt[i]) {
                case '\0':
                    sentinel("Invalid format, you ended with %%.");
                    break;
                case 'd':
                    out_int = va_arg(argp, int *);
                    rc = read_int(out_int);
                    check(rc == 0, "Failed to read int.");
                    break;
                case 'c':
                    out_char = va_arg(argp, char *);
                    *out_char = fgetc(stdin);
                    break;
                case 's':
                    max_buffer = va_arg(argp, int);
                    out_string = va_arg(argp, char **);
                    rc = read_string(out_string, max_buffer);
                    check(rc == 0, "Failed to read string.");
                    break;

                default:
                    sentinel("Invalid format.");
            }
        } else {
            fgetc(stdin);
        }

        check(!feof(stdin) && !ferror(stdin), "Input error.");
    }

    va_end(argp);
    return 0;

    error:
    va_end(argp);
    return -1;
}

int print_args(const char *fmt, ...)
{
    int i = 0;
    int *out_int = NULL;
    char *out_char = NULL;
    char **out_string = NULL;

    va_list(argp);
    va_start(argp, fmt);

    for (i = 0; fmt[i] != '\0'; ++i) {
        switch (fmt[i]) {
            case '\0':
                sentinel("Hit null char case.");
                break;
            case 'd':
                out_int = va_arg(argp, int *);
                printf("%d", *out_int);
                break;
            case 'c':
                out_char = va_arg(argp, char *);
                printf("%c", *out_char);
                break;
            case 's':
                out_string = va_arg(argp, char **);
                check(out_string != NULL, "out_string still NULL");
                printf("%s", *out_string);
                break;
            default:
                sentinel("Invalid format.");
        }
    }

    va_end(argp);
    return 0;

    error:
    va_end(argp);
    return -1;
}

int main(int argc, char *argv[])
{
    char *first_name = NULL;
    char initial = ' ';
    char *last_name = NULL;
    int age = 0;

    char *first_name_q = "What's your first name? ";
    print_args("s", &first_name_q);
    int rc = read_scan("%s", MAX_DATA, &first_name);
    // int rc = read_scan("%s", &first_name);
    check(rc == 0, "Failed first name.");

    char *initial_q = "What's your initial? ";
    print_args("s", &initial_q);
    rc = read_scan("%c\n", &initial);
    check(rc == 0, "Failed initial.");

    char *last_name_q = "What's your last name? ";
    print_args("s", &last_name_q);
    rc = read_scan("%s", MAX_DATA, &last_name);
    // rc = read_scan("%s", &last_name);
    check(rc == 0, "Failed last name.");

    char *age_q = "How old are you? ";
    print_args("s", &age_q);
    rc = read_scan("%d", &age);

    char *results_h = "---- RESULTS ----\n";
    char *first_name_h = "First Name: ";
    char *initial_h = "Initial: ";
    char *last_name_h = "Last Name: ";
    char *age_h = "Age: ";
    char nl = '\n';
    print_args("ssssccsssdc", &results_h, &first_name_h, &first_name,
                                            &initial_h, &initial, &nl,
                                            &last_name_h, &last_name,
                                            &age_h, &age, &nl);

    free(first_name);
    free(last_name);
    return 0;

    error:
    return -1;
}
