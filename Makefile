CFLAGS=-Wall -g -I.

ex19/ex19: ex19/object.o
ex20/ex20: dbg.h
ex22/ex22_main: dbg.h ex22/ex22.o
ex22/ex22_main-ec: dbg.h ex22/ex22.o
ex25/ex25: dbg.h
ex25/ex25-ec: dbg.h

clean:
	rm -f ex1-ex18/ex1
	rm -f ex1-ex18/ex3
	rm -f ex1-ex18/ex4
	rm -f ex1-ex18/ex5 rm -f ex1-ex18/ex6
	rm -f ex1-ex18/ex6-ec
	rm -f ex1-ex18/ex7
	rm -f ex1-ex18/ex7-ec
	rm -f ex1-ex18/ex8
	rm -f ex1-ex18/ex8-ec
	rm -f ex1-ex18/ex9
	rm -f ex1-ex18/ex9-ec
	rm -f ex1-ex18/ex10
	rm -f ex1-ex18/ex10-ec
	rm -f ex1-ex18/ex11
	rm -f ex1-ex18/ex11-ec
	rm -f ex1-ex18/ex12
	rm -f ex1-ex18/ex13
	rm -f ex1-ex18/ex13-ec
	rm -f ex1-ex18/ex14
	rm -f ex1-ex18/ex14-ec
	rm -f ex1-ex18/ex15
	rm -f ex1-ex18/ex15-ec
	rm -f ex1-ex18/ex13-ec15
	rm -f ex1-ex18/ex14-ec15
	rm -f ex1-ex18/ex15
	rm -f ex1-ex18/ex16
	rm -f ex1-ex18/ex16-ec
	rm -f ex1-ex18/ex17
	rm -f ex1-ex18/ex17-ec
	rm -f ex1-ex18/ex17-ec-global
	rm -f ex1-ex18/ex18
	rm -f ex1-ex18/ex18-ec
	rm -f ex19/ex19
	rm -f ex19/object.o
	rm -f ex20/ex20
	rm -f ex20/ex20-ec
	rm -f ex22/ex22.o
	rm -f ex22/ex22_main
	rm -f ex23/ex23
	rm -f ex23/ex23-ec
	rm -f ex25/ex25
	rm -f ex25/ex25-ec
	rm -f ex31/ex31
