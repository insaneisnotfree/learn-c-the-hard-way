#include <stdio.h>
#include "dbg.h"
#include <dlfcn.h>

#define LIB_FILE "build/libex29.so"

typedef int (*lib_function)(const char *data);

int run_func(char * const lib_file, char * const func_to_run, char * const data)
{
    void *lib = dlopen(lib_file, RTLD_NOW);
    check(lib != NULL, "Failed to open the library %s: %s", lib_file, dlerror());

    lib_function func = dlsym(lib, func_to_run);
    check(func != NULL, "Did not find %s function in the library %s: %s",
            func_to_run, lib_file, dlerror());

    int rc;
    rc = func(data);
    check(rc == 0, "Function %s return %d for data: %s", func_to_run, rc, data);

    rc = dlclose(lib);
    check(rc == 0, "Failed to close %s", lib_file);

    return 0;

error:
    return 1;
}

int main()
{
    int rc;
    rc = run_func(LIB_FILE, "print_a_message", "hello there");
    check(rc == 0, "Failed to run print_a_message");

    rc = run_func(LIB_FILE, "uppercase", "hello there");
    check(rc == 0, "Failed to run uppercase");

    rc = run_func(LIB_FILE, "lowercase", "HELLO tHeRe");
    check(rc == 0, "Failed to run lowercase");

    rc = run_func(LIB_FILE, "fail_on_purpose", "i fail");
    check(rc != 0, "Successfully ran fail_on_purpose");
    rc = 0;

    rc = run_func(LIB_FILE, "asdafa", "not there");
    check(rc != 0, "Successfully ran non-existent function");
    rc = 0;

    rc = run_func("../build/libex.so", "uppercase", "should fail");
    check(rc != 0, "Successfully ran with non-existent .so library");

    return 0;

error:
    return 1;
}
